import {Component, ElementRef} from '@angular/core';
import {LoginService} from '../../services/login.service'
// import { User } from '../../models/model-index'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';

@Component({
    selector: 'login-form',
    providers: [LoginService],
    templateUrl: 'login.html',
    styleUrls: ['login.css']
})

export class Login {
  
  public errorMsg = '';
  users: any = [];
  loginForm: FormGroup;

  email: string = null;
  password: string = null;

  constructor(private _service: LoginService,
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    this._buildLoginForm();
  }

  private _buildLoginForm(){
    this.loginForm = this._formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  getCredentials(values){
    console.log("GET CREDENTIALS:------", values);
  }
}