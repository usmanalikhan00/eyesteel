// const {app, BrowserWindow, ipcMain} = require('electron')

// var print_win;

// app.on('ready', ()=>{
//     var mainWindow = new BrowserWindow({
//         width: 1280,
//         height: 720
//     })
//     // mainWindow.loadURL('file://' + __dirname + '/src/index.html');
//     mainWindow.loadURL('http://localhost:4200')
//     mainWindow.webContents.openDevTools()
// });

// app.on('window-all-closed', () => {
//   app.quit()
// });


const {app, BrowserWindow} = require('electron')
const path = require('path');
const url = require('url');

require('dotenv').config();

let win = null;

app.on('ready', function () {

  // Initialize the window to our specified dimensions
  win = new BrowserWindow({width: 1000, height: 600});

  // Specify entry point
  // win.loadURL('http://localhost:4200');

  // if (process.env.PACKAGE === 'true'){
  //   win.loadURL(url.format({
  //     pathname: path.join(__dirname, 'dist/index.html'),
  //     protocol: 'file:',
  //     slashes: true
  //   }));
  //   win.webContents.openDevTools();
  //   console.log("__dirname", __dirname);
  // } else {
  //   win.loadURL(process.env.HOST);
  //   win.webContents.openDevTools();
  // }
  win.loadURL('file://' + __dirname + '/dist/index.html');
  // Show dev tools
  // Remove this line before distributing
  win.webContents.openDevTools()

  // Remove window once app is closed
  win.on('closed', function () {
    win = null;
  });

});

app.on('activate', () => {
  if (win === null) {
    createWindow()
  }
})

app.on('window-all-closed', function () {
  if (process.platform != 'darwin') {
    app.quit();
  }
});