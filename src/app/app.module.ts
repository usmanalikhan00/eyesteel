import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginService } from './services/login.service'
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { ROUTES } from './app.routes';
import { AuthGuard } from './_gaurds/auth.gaurd';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { MatButtonModule, 
         MatCheckboxModule, 
         MatInputModule, 
         MatIconModule, 
         MatFormFieldModule, 
         MatSelectModule,
         MatCardModule,
         MatGridListModule,
         MatToolbarModule,
         MatTooltipModule,
         MatSidenavModule,
         MatListModule,
         MatTableModule, 
         MatAutocompleteModule, 
         MatPaginatorModule,
         MatRadioModule } from '@angular/material';
import { Login, 
         deodashboard, 
         userdashboard, 
         admindashboard, 
         products,
         addproduct,
         adminsettings,
         productcategories,
         subcategories,
         addInvoice,
         invoice,
         customers,
         addcustomer,
         cashsettings,
         subAccountType,
         storesettings,
         singleinvoice,
         purchase,
         addpurchase,
         singlepurchase,
         companyinfo } from './components/component-index';

@NgModule({
  declarations: [
    AppComponent,
    Login,
    deodashboard,
    userdashboard,
    admindashboard,
    products,
    addproduct,
    adminsettings,
    productcategories,
    subcategories,
    addInvoice,
    invoice,
    customers,
    addcustomer,
    cashsettings,
    subAccountType,
    storesettings,
    singleinvoice,
    purchase,
    addpurchase,
    singlepurchase,
    companyinfo
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(ROUTES, { useHash: true }),
    FormsModule, 
    ReactiveFormsModule,
    MatButtonModule, 
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatIconModule,
    MatCardModule,
    MatGridListModule,
    MatToolbarModule,
    MatTooltipModule,
    MatSidenavModule,
    MatListModule,
    MatTableModule,
    MatRadioModule,
    MatAutocompleteModule,
    MatPaginatorModule
  ],
  exports: [addproduct],
  providers: [AuthGuard, LoginService],
  bootstrap: [AppComponent]
})
export class AppModule { }
