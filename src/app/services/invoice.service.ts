import { Injectable }  from '@angular/core';
import { Router } from  '@angular/router';
// import { User } from '../models/model-index'
import * as PouchDB  from 'pouchdb'
import * as PouchFind from 'pouchdb-find'
PouchDB.plugin(PouchFind)
// import * as moment from "moment";

@Injectable()
export class InvoiceService {
  
  authenticatedUser: any = [];
  public invoicesDB = new PouchDB('steelcustomerinvoice');
  

  constructor(private _router: Router){}

  addInvoice(invoiceDoc){
    console.log("INVOICE DOCUMENT TO PUT:-----", invoiceDoc)
    var self = this;
    return self.invoicesDB.put({
        _id: new Date().toISOString(),
        invoicenumber: invoiceDoc.invoicenumber, 
        invoicetotal: invoiceDoc.invoicetotal, 
        invoiceproducts: invoiceDoc.invoiceproducts, 
        invoicecustomer: invoiceDoc.invoicecustomer, 
        status: invoiceDoc.status, 
        createdby: invoiceDoc.createdby, 
        createdat: invoiceDoc.createdat,
        invoicenotes: invoiceDoc.invoicenotes
    });
  }

  allInvocies(){
    var self = this;
    return self.invoicesDB.allDocs({
      include_docs: true,
      attachments: true,
      'endkey':'_design'
    });
  }

  getSingleInvoice(invoiceId){
    var self = this;
    return self.invoicesDB.createIndex({
      index: {
        fields: ['_id']
      }
    }).then(function(){
      return self.invoicesDB.find({
        selector: {_id:{$eq: invoiceId}}
      });
    })
  }


}