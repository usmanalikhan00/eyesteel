import { Injectable }  from '@angular/core';
import { Router } from  '@angular/router';
// import { User } from '../models/model-index'
import * as PouchDB  from 'pouchdb'
import * as PouchFind from 'pouchdb-find'
import * as pouchdbUpsert from 'pouchdb-upsert';
PouchDB.plugin(PouchFind)
PouchDB.plugin(pouchdbUpsert);
// PouchDB.plugin(require('pouchdb-upsert'))
// import * as moment from "moment";

@Injectable()
export class ProductService {
  
  authenticatedUser: any = [];
  public productsDB = new PouchDB('steelproducts');
  public productCategoriesDB = new PouchDB('productcategories');
  public productSubCategoriesDB = new PouchDB('productsubcategories');

  constructor(private _router: Router){
  }

  addProduct(values){
    var self = this;
    return self.productsDB.put({
      _id: new Date().toISOString(),
      name: values.name,
      batchnumber: values.batchnumber,
      weight: values.weight,
      price: values.price,
      category: values.category,
      stockvalue: values.stockvalue,
      type: values.type,
      subcategory: values.subcategory,
      stores: values.stores,
      storeStocks: values.storeStocks,
      netweight: values.netweight,
      networth: values.networth
    });
  }

  allProducts(){
    var self = this;
    return self.productsDB.allDocs({
      include_docs: true,
      attachments: true
    });
  }

  addProductCategory(name){
    var self = this;  
    return self.productCategoriesDB.put({
      _id: new Date().toISOString(),
      name: name
    })
  }

  addProductSubCategory(name, catgeoryId){
    var self = this;  
    return self.productSubCategoriesDB.put({
      _id: new Date().toISOString(),
      name: name,
      categoryId: catgeoryId
    })
  }

  
  allProductCategories(){
    var self = this;
    return self.productCategoriesDB.allDocs({
      include_docs: true,
      attachments: true,
      'endkey': '_design'
    });
  }
  
  allProductSubCategories(){
    var self = this;
    return self.productSubCategoriesDB.allDocs({
      include_docs: true,
      attachments: true,
      'endkey': '_design'
    });
  }
  


  getSingleCategory(categoryId){
    var self = this;
    return self.productCategoriesDB.createIndex({
      index: {
        fields: ['_id']
      }
    }).then(function(){
      return self.productCategoriesDB.find({
        selector: {_id:{$eq: categoryId}}
      });
    })
  }



  getSingleCategoryItems(categoryId){
    var self = this;
    return self.productSubCategoriesDB.createIndex({
      index: {
        fields: ['categoryId']
      }
    }).then(function(){
      return self.productSubCategoriesDB.find({
        selector: {categoryId: {$eq: categoryId}}
      });
    })
  }

  updateInvoiceProducts(products){
    var self = this
    var docsToUpdate: any = []
    for (let item of products){
      var object = {
        '_id': item._id,
        '_rev': item._rev,
        'name': item.name,
        'batchnumber': item.batchnumber,
        'weight': item.weight,
        'price': item.price,
        'category': item.category,
        'stockvalue': item.stockvalue,
        'type': item.type,
        'subcategory': item.subcategory,
        'stores': item.stores,
        'storeStocks': item.storeStocks,
        'netweight': item.netweight,
        'networth': item.networth
      }
      docsToUpdate.push(object)
      // console.log("PRODUCT TO UPDATE:---", object)
      // self.productsDB.upsert(object._id, self.myDeltaFunction)
    }
    return self.productsDB.bulkDocs(docsToUpdate)
    

    // return self.productsDB.upsert(docsToUpdate)
    // return true
  }
  // myDeltaFunction(doc) {
  //   doc.counter = doc.counter || 0;
  //   doc.counter++;
  //   return doc;
  // }

}