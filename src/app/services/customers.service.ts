import { Injectable }  from '@angular/core';
import { Router } from  '@angular/router';
// import { User } from '../models/model-index'
import * as PouchDB  from 'pouchdb'
import * as PouchFind from 'pouchdb-find'
PouchDB.plugin(PouchFind)
// import * as moment from "moment";

@Injectable()
export class CustomersService {
  
  selectedCustomer: any = [];
  public customersDB = new PouchDB('steelcustomers');
  // public productCategoriesDB = new PouchDB('productcategories');
  // public productSubCategoriesDB = new PouchDB('productsubcategories');

  constructor(private _router: Router){}

  addCustomer(values){
    var self = this;
    return self.customersDB.put({
      _id: new Date().toISOString(),
      name: values.name,
      address: values.address,
      email: values.email,
      phone: values.phone,
      fax: values.fax,
      balance:0
    });
  }

  allCustomers(){
    var self = this;
    return self.customersDB.allDocs({
      include_docs: true,
      attachments: true
    });
  }

  updateCustomerBalance(){

  }
  
  // addProductCategory(name){
  //   var self = this;  
  //   return self.productCategoriesDB.put({
  //     _id: new Date().toISOString(),
  //     name: name
  //   })
  // }

  // addProductSubCategory(name, catgeoryId){
  //   var self = this;  
  //   return self.productSubCategoriesDB.put({
  //     _id: new Date().toISOString(),
  //     name: name,
  //     categoryId: catgeoryId
  //   })
  // }

  
  // allProductCategories(){
  //   var self = this;
  //   return self.productCategoriesDB.allDocs({
  //     include_docs: true,
  //     attachments: true,
  //     'endkey': '_design'
  //   });
  // }
  
  // allProductSubCategories(){
  //   var self = this;
  //   return self.productSubCategoriesDB.allDocs({
  //     include_docs: true,
  //     attachments: true,
  //     'endkey': '_design'
  //   });
  // }
  


  getSingleCustomer(customerId){
    var self = this;
    return self.customersDB.createIndex({
      index: {
        fields: ['_id']
      }
    }).then(function(){
      return self.customersDB.find({
        selector: {_id:{$eq: customerId}}
      });
    })
  }



  // getSingleCategoryItems(categoryId){
  //   var self = this;
  //   return self.productSubCategoriesDB.createIndex({
  //     index: {
  //       fields: ['categoryId']
  //     }
  //   }).then(function(){
  //     return self.productSubCategoriesDB.find({
  //       selector: {categoryId: {$eq: categoryId}}
  //     });
  //   })
  // }

}