import { Injectable }  from '@angular/core';
import { Router } from  '@angular/router';
// import { User } from '../models/model-index'
import * as PouchDB  from 'pouchdb';
// import * as PouchFind from 'pouchdb-find'
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";

// var users = [
//   new User('1','aajeel','admin@aajeel.com','adm9', 'admin'),
//   new User('2','khan','operator@aajeel.com','ope9', 'operator')
// ];

@Injectable()
export class LoginService {
  
  authenticatedUser: any = [];
  public usersDB = new PouchDB('users');
  

  constructor(private _router: Router){}

  addUsers(){
    var self = this;
    return self.usersDB.bulkDocs([
              {_id: 'admin', email : 'admin', password: 'admin'},
              {_id: 'user', email : 'user', password: 'user'},
              {_id: 'deo', email : 'deo', password: 'deo'},
              {_id: 'superadmin', email : 'superadmin', password: 'superadmin'}
           ]);
  }

  checkCredentials(values){
    var self = this;
    return self.usersDB.createIndex({
      index: {
        fields: ['email', 'password']
      }
    }).then(function(){
      return self.usersDB.find({
        selector: {email:{$eq: values.email}, password:{$eq: values.password}}
      });
    })
  }

  loggedIn(){
    if (localStorage.getItem('user') !== null){
      return true;
    }else{
      return false;
    }
  }

  logout(){
    var self = this;
    localStorage.removeItem('user')
    // localStorage.removeItem('userRole')
    self._router.navigate(['login'])
  }

}