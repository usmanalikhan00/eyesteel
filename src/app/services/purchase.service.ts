import { Injectable }  from '@angular/core';
import { Router } from  '@angular/router';
// import { User } from '../models/model-index'
import * as PouchDB  from 'pouchdb'
import * as PouchFind from 'pouchdb-find'
PouchDB.plugin(PouchFind)
// import * as moment from "moment";

@Injectable()
export class PurchaseService {
  
  authenticatedUser: any = [];
  public purchaseDB = new PouchDB('steelcustomerpurchase');
  

  constructor(private _router: Router){}

  addPurchase(purchaseDoc){
    console.log("PURCHASE DOCUMENT TO PUT:-----", purchaseDoc)
    var self = this;
    return self.purchaseDB.put({
        _id: new Date().toISOString(),
        purchasenumber: purchaseDoc.purchasenumber, 
        purchasetotal: purchaseDoc.purchasetotal, 
        purchaseproducts: purchaseDoc.purchaseproducts, 
        purchasecustomer: purchaseDoc.purchasecustomer, 
        status: purchaseDoc.status, 
        createdby: purchaseDoc.createdby, 
        createdat: purchaseDoc.createdat,
        purchasenotes: purchaseDoc.purchasenotes
    });
  }

  allPurchases(){
    var self = this;
    return self.purchaseDB.allDocs({
      include_docs: true,
      attachments: true,
      'endkey':'_design'
    });
  }

  getSinglePurchase(purchaseId){
    var self = this;
    return self.purchaseDB.createIndex({
      index: {
        fields: ['_id']
      }
    }).then(function(){
      return self.purchaseDB.find({
        selector: {_id:{$eq: purchaseId}}
      });
    })
  }


}