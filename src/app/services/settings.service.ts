import { Injectable }  from '@angular/core';
import { Router } from  '@angular/router';
// import { User } from '../models/model-index'
import * as PouchDB  from 'pouchdb';
import * as PouchFind from 'pouchdb-find'
PouchDB.plugin(PouchFind)
// import * as moment from "moment";

// var users = [
//   new User('1','aajeel','admin@aajeel.com','adm9', 'admin'),
//   new User('2','khan','operator@aajeel.com','ope9', 'operator')
// ];

@Injectable()
export class SettingsService {
  
  // authenticatedUser: any = [];
  public companyInfoDB = new PouchDB('companyinfo');
  

  constructor(private _router: Router){}

  addCompanyInfo(values){
    var self = this;
    return self.companyInfoDB.put({
      _id: new Date().toISOString(),
      name: values.name,
      intro: values.intro,
      website: values.website,
      address: values.address,
      phone: values.phone,
      ntn: values.ntn,
      dealsin: values.dealsin
    })
  }
  
  getCompnayInfo(){
    var self = this;
    return self.companyInfoDB.allDocs({
      include_docs: true,
      attachments: true
    });
  }  

  updateCompanyInfo(values, companyId, companyRev){
    var self = this;
    if (companyId){
      return self.companyInfoDB.put({
        _id: companyId,
        _rev: companyRev,
        name: values.name,
        intro: values.intro,
        website: values.website,
        address: values.address,
        phone: values.phone,
        ntn: values.ntn,
        dealsin: values.dealsin
      })
    }else{
      return self.companyInfoDB.put({
        _id: new Date().toISOString(),
        name: values.name,
        intro: values.intro,
        website: values.website,
        address: values.address,
        phone: values.phone,
        ntn: values.ntn,
        dealsin: values.dealsin
      })
    }
  }
}