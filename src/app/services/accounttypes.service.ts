import { Injectable }  from '@angular/core';
import { Router } from  '@angular/router';
// import { User } from '../models/model-index'
import * as PouchDB  from 'pouchdb';
import * as PouchFind from 'pouchdb-find'
PouchDB.plugin(PouchFind)
// import * as moment from "moment";

// var users = [
//   new User('1','aajeel','admin@aajeel.com','adm9', 'admin'),
//   new User('2','khan','operator@aajeel.com','ope9', 'operator')
// ];

@Injectable()
export class AccountTypesService {
  
  // authenticatedUser: any = [];
  public accountTypesDB = new PouchDB('steelaccounttypes');
  public subAccountsDB = new PouchDB('steelsubaccounts');
  

  constructor(private _router: Router){}

  addAccountType(values){
    var self = this;
    return self.accountTypesDB.put({
      _id: new Date().toISOString(),
      name: values.name,
      type: values.type
    })
  }
  
  getAccountTypes(){
    var self = this;
    return self.accountTypesDB.allDocs({
      include_docs: true,
      attachments: true,
      'endkey': '_design'
    });
  }  

  updateAccountType(values, accountId, accountRev){
    var self = this;
    if (accountId){
      return self.accountTypesDB.put({
        _id: accountId,
        _rev: accountRev,
        name: values.name
      })
    }else{
      return self.accountTypesDB.put({
        _id: new Date().toISOString(),
        name: values.name
      })
    }
  }

  addSubAccount(name, accountId, type){
    var self = this;  
    return self.subAccountsDB.put({
      _id: new Date().toISOString(),
      name: name,
      accountId: accountId
    })
  }

  getSingleAccount(accountId){
    var self = this;
    return self.accountTypesDB.createIndex({
      index: {
        fields: ['_id']
      }
    }).then(function(){
      return self.accountTypesDB.find({
        selector: {_id:{$eq: accountId}}
      });
    })
  }



  getSingleAccountItems(accountId){
    var self = this;
    return self.subAccountsDB.createIndex({
      index: {
        fields: ['accountId']
      }
    }).then(function(){
      return self.subAccountsDB.find({
        selector: {accountId: {$eq: accountId}}
      });
    })
  }
}