import { Routes, RouterModule } from '@angular/router';
import { Login, 
         deodashboard,
         userdashboard,
         admindashboard,
         products,
         addproduct,
         adminsettings,
         productcategories,
         subcategories,
         addInvoice,
         invoice,
         customers,
         cashsettings,
         subAccountType,
         addcustomer,
         storesettings,
         singleinvoice,
         purchase,
         addpurchase,
         singlepurchase,
         companyinfo } from './components/component-index';
import { AuthGuard } from './_gaurds/auth.gaurd'



export const ROUTES: Routes = [
  { path: '',      
    component: Login 
  },
  {
    path: 'deodashboard',
    component: deodashboard,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'userdashboard',
    component: userdashboard,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'admindashboard',
    component: admindashboard,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'products',
    component: products,
    canActivate: [ AuthGuard ]
  },  
  {
    path: 'addproduct',
    component: addproduct,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'adminsettings',
    component: adminsettings,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'productcategories',
    component: productcategories,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'subcategories/:categoryId',
    component: subcategories,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'subaccounts/:accountId',
    component: subAccountType,
    canActivate: [ AuthGuard ]
  },

  {
    path: 'companyinfo',
    component: companyinfo,
    canActivate: [ AuthGuard ]
  },

  {
    path: 'invoice',
    component: invoice,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'addinvoice',
    component: addInvoice,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'invoice/:invoiceId',
    component: singleinvoice,
    canActivate: [ AuthGuard ]
  },
  

  {
    path: 'purchase',
    component: purchase,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'addpurchase',
    component: addpurchase,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'purchase/:purchaseId',
    component: singlepurchase,
    canActivate: [ AuthGuard ]
  },
  

  {
    path: 'customers',
    component: customers,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'addcustomer',
    component: addcustomer,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'cashsettings',
    component: cashsettings,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'storesettings',
    component: storesettings,
    canActivate: [ AuthGuard ]
  },
  
  { path: 'login',  
    component: Login 
  }
];
