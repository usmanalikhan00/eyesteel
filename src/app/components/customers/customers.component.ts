import { Component, ElementRef, ViewChild } from '@angular/core';
import { LoginService } from '../../services/login.service'
import { ProductService } from '../../services/product.service'
import { CustomersService } from '../../services/customers.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';


@Component({
    selector: 'customers',
    providers: [LoginService, ProductService, CustomersService],
    templateUrl: 'customers.html',
    styleUrls: ['customers.css']
})

export class customers {
  
  public customersDB = new PouchDB('steelcustomers');

  customers: any = [];
  addCustomerForm: FormGroup;
  addCustoemrFlag: boolean = false;
  allCustomers: any= [];

  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _customersService: CustomersService, 
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    this._buildAddCustomerForm();
  }


  private _buildAddCustomerForm(){

  }

  ngOnInit(){
    var self = this;
    self.allCustomers = [];
    self._customersService.allCustomers().then(function(result){
      result.rows.map(function (row) { 
        self.allCustomers.push(row.doc); 
      });
      console.log("ALL CUSTOMERS:=====", self.allCustomers);
    }).catch(function(err){
      console.log(err);
    })
  }

  addCustomer(){
    console.log("ADD PRODUCT CALLED:--");
    this._router.navigate(['addcustomer'])
  }

  logout(){
    this._loginService.logout()
  }  
}
