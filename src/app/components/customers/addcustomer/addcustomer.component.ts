import { Component, ElementRef, ViewChild } from '@angular/core';
import { LoginService } from '../../../services/login.service'
import { ProductService } from '../../../services/product.service'
import { CustomersService } from '../../../services/customers.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import {DataSource} from '@angular/cdk/collections';
import {MatPaginator} from '@angular/material';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';

@Component({
    selector: 'add-customer',
    providers: [LoginService, 
                ProductService,
                CustomersService],
    templateUrl: 'addcustomer.html',
    styleUrls: ['addcustomer.css']
})

export class addcustomer {
  
  public customersDB = new PouchDB('steelcustomers');

  addCustomerForm: FormGroup;
  allCustomers: any = [];
  // foods = [
  //   {value: 'steak-0', viewValue: 'Steak'},
  //   {value: 'pizza-1', viewValue: 'Pizza'},
  //   {value: 'tacos-2', viewValue: 'Tacos'}
  // ];
  // allCategories: any = [];
  // allSubCategories: any = [];
  // selectedCategory: any;

  constructor(private _loginService: LoginService,
              private _productService: ProductService,
              private _customersService: CustomersService,
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    this._buildAddCustomerForm();
    // var changes = this.productsDB.changes({
    //   since: 'now',
    //   live: true,
    //   include_docs: true
    // }).on('change', function(change) {
    //   // handle change
    //   console.log("something changed", change);
    // }).on('complete', function(info) {
    //   // changes() was canceled
    //   console.log("something completed", info);
    // }).on('error', function (err) {
    //   console.log(err);
    // })
  }

  // public changes = this.productsDB.changes({
  //   since: 'now',
  //   live: true,
  //   include_docs: true
  // }).on('change', function(change) {
  //   // handle change
  //   console.log("something changed", change);
  // }).on('complete', function(info) {
  //     console.log("something changed", info);
  //   // changes() was canceled
  // }).on('error', function (err) {
  //   console.log(err);
  // });

  private _buildAddCustomerForm(){
    this.addCustomerForm = this._formBuilder.group({
      name: ['', Validators.required],
      address: [''],
      email: [''],
      phone: [''],
      fax: ['']
    })
  }

  ngOnInit(){
    var self = this;
    // self._productService.allProductCategories().then(function(result){
    //   self.allCategories = []
    //   result.rows.map(function(row){
    //     // row.doc[0].stockprice = row.doc[0].price * row.doc[0].stockvalue
    //     self.allCategories.push(row.doc)
    //   })
    //   console.log("Result from all product Categories:===", self.allCategories)
    //   // self._productService.allProductSubCategories().then(function(result){
    //   //   console.log("Result from all product sub==categories:===", result)
    //   // }).catch(function(err){
    //   //   console.log(err)
    //   // })
    // }).catch(function(err){
    //   console.log(err)
    // })
  }

  addCustomer(values){
    var self = this;
    console.log("ADD PRODUCT CALLED:--", values);
    // values.categoryname = 
    // values.subcategoryname = 
    self._customersService.addCustomer(values).then(function(result){
      console.log("Customer ADDED:===", result);
        self.addCustomerForm.reset();
        // self.addProductForm.clearValidators();
      // console.log("CHANGE VARIACLE", changes);
    }).catch(function(err){
      console.log(err);
    })
  }

  // selectSubCategoty($event){
  //   var self = this;
  //   console.log("Category selected", $event);
  //   self.selectedCategory = $event.value
  //   self._productService.getSingleCategoryItems(self.selectedCategory).then(function(result){
  //     self.allSubCategories = [];
  //       // self.selectedCategory = doc;
  //     for (let row of result.docs){
  //       self.allSubCategories.push(row)
  //     }
  //   }).catch(function(err){
  //     console.log(err)
  //   })
  // }

  goBack(){
    this._router.navigate(['customers'])
  }

  logout(){
    this._loginService.logout()
  }  
}