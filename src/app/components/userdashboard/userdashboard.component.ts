import {Component, ElementRef} from '@angular/core';
import {LoginService} from '../../services/login.service'
// import { User } from '../../models/model-index'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';

@Component({
    selector: 'user-dashboard',
    providers: [LoginService],
    templateUrl: 'userdashboard.html',
    styleUrls: ['userdashboard.css']
})

export class userdashboard {
  
  public errorMsg = '';
  public userDB = new PouchDB('users');

  users: any = [];
  addProductForm: FormGroup;

  email: string = null;
  password: string = null;

  constructor(private _loginService: LoginService,
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    this._buildAddProductForm();

  }

  private _buildAddProductForm(){

  }

  logout(){
    this._loginService.logout()
  }
}