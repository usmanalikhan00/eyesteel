import { Component, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
// import { ChangeDetectionRef } from '@angular/common';
import { LoginService } from '../../services/login.service'
import { ProductService } from '../../services/product.service'
import { StoreSettingsService } from '../../services/storesettings.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb';
import * as _ from 'lodash'
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import { UUID } from 'angular2-uuid';

@Component({
    selector: 'products',
    providers: [LoginService, ProductService, StoreSettingsService],
    templateUrl: 'products.html',
    styleUrls: ['products.css']
})

export class products {
  
  // public productsDB = new PouchDB('products');
  public productsDB = new PouchDB('steelproducts');

  users: any = [];
  addProductFlag: boolean = false;
 public invoiceForm: FormGroup;

  addProductForm: FormGroup;
  allProducts: any = [];

  allCategories: any = [];
  allStores: any = [];
  allSubCategories: any = [];
  selectedStores: any = [];
  storeCount: any = 0;
  selectedCategory: any;
  enableProductForm: boolean = false
  stockFlag: boolean = false
  productTypes = [
    'Weighted',
    'Single Unit'
  ];

  toppings = new FormControl();
  toppingList = [];

  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _storeSettingsService: StoreSettingsService, 
              private _cdr: ChangeDetectorRef, 
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    this._buildAddProductForm();
  }

  private _buildAddProductForm(){
    
    this.addProductForm = this._formBuilder.group({
      name: ['', Validators.required],
      price: ['', Validators.required],
      weight: ['', Validators.required],
      category: ['', Validators.required],
      subcategory: [''],
      stores: [''],
      // storeStocks: [''],
      type: ['', Validators.required]
    })

  }

  ngOnInit(){
    var self = this;
    PouchDB.replicate(self.productsDB, 'http://localhost:5984/steelproducts', {live: true});
    self.getAllProducts()
    self.getAllCategories()
    self.getAllStores()

    // this.invoiceForm = this._formBuilder.group({
    //   itemRows: this._formBuilder.array([this.initItemRows()])
    // });

  }

  // initItemRows() {
  //     return this._formBuilder.group({
  //         itemname: ['']
  //     });
  // }

  // addNewRow() {
  //     const control = <FormArray>this.invoiceForm.controls['itemRows'];
  //     control.push(this.initItemRows());
  // }

  // deleteRow(index: number) {
  //     const control = <FormArray>this.invoiceForm.controls['itemRows'];
  //     control.removeAt(index);
  // }

  getAllStores(){
    var self = this
    self.allStores = []
    self._storeSettingsService.allStores().then(function(result){
      result.rows.map(function (row) { 
        self.allStores.push(row.doc); 
      });
      // self.toppingList = self.allStores
      self.toppingList = []
      for (let store of self.allStores){
        var doc = {
          name:store.name,
          stock:null,
          _id:store._id,
          _rev:store._rev
        }
        self.toppingList.push(doc)        
      }

      console.log("ALL Stores:=====", self.allStores);
    }).catch(function(err){

    })
  }

  multiSelectStore(values){
    console.log("MULTISELECT CHANGE CALLED", values)
    // this.selectedStores = values.value
    this.selectedStores = []
    for (let store of values.value){
      this.selectedStores.push(store)
    }
  }


  getProductType($event){
    console.log("PRODUCT TYPE CHANGE CALLED:-------", $event.value)
    if ($event.value === 'Weighted'){
      (<FormGroup>this.addProductForm).patchValue({'weight':''}, { onlySelf: true });
    }else if ($event.value === 'Single Unit'){
      (<FormGroup>this.addProductForm).patchValue({'weight':'none'}, { onlySelf: true });
    }
  }

  setProductStore($event){
    // setValue({'store':name}, { onlySelf: true });
    (<FormGroup>this.addProductForm).patchValue({'stores':''}, { onlySelf: true });
    // (<FormGroup>this.addProductForm).;
    var tempStore = $event.value
    var productStore: any = null
    console.log("RESET STORE EVENT", tempStore)
    for (let item of this.allStores){
      if (item._id === tempStore){
        productStore = item
      }
    }
    console.log("PRODUCT STORE TO PUSH", productStore)
    var existFlag: boolean = false
    if (this.selectedStores.length === 0){
      console.log("RESET STORE EVENT First", tempStore)
      productStore.stock = null
      this.selectedStores.push(productStore)
      this.stockFlag = true
    }else{
      console.log("RESET STORE EVENT SECONDNNNNN", productStore)
      for (let i=0; i<this.selectedStores.length; i++){
        var key = this.selectedStores[i]
        if (key._id == productStore._id){
          key.stock = productStore.stock
          existFlag = true
          break
        }else{
          existFlag = false
        }

      }
      if(!existFlag){
        productStore.stock = null
        this.selectedStores.push(productStore)
        this.stockFlag = true
      }else{
        console.log("VALUES EXISTS")
      }
    }
    this._cdr.detectChanges()
    console.log("ALL STORES TO SAVE", this.selectedStores)
  }


  changeStoreStocks($event, store){
    // store.stock = stock.toString()
    console.log("ALL STORES AFETR STOCK CHANGE:---", store, $event)
    console.log("ALL STORES AFETR CHANGING:---", this.selectedStores)

  }

  getAllProducts(){

    var self = this;
    self.allProducts = [];
    self._productService.allProducts().then(function(result){
      result.rows.map(function (row) { 
        self.allProducts.push(row.doc); 
      });
      console.log("ALL PRODUCTS:=====", self.allProducts);
    }).catch(function(err){
      console.log(err);
    })

  }

  addProduct(values){

    var self = this;
    
    var ID = Math.floor(Math.random()*100000000) + 800000000
    console.log("THE GENERATE NUMBER 3:-----", ID)
    values.batchnumber = ID
    console.log("ADD PRODUCT CALLED:--", values, self.selectedStores);
    values.stores = self.selectedStores
    var stockCount = 0
    for (let store of self.selectedStores){
      stockCount = stockCount + store.stock
    }
    values.stockvalue = stockCount
    values.storeStocks = stockCount

    if (values.type === 'Weighted'){
      values.netweight = values.weight * values.stockvalue
      values.networth = values.netweight * values.price
    }else if (values.type === 'Single Unit'){
      values.netweight = ""
      values.networth = values.stockvalue * values.price

    }

    for (let item of self.selectedStores){
      if (item.stock === null){
        self.stockFlag = true
        // console.log("STOCK FLAG TRUE:---", this.stockFlag)
        break
      }else{
        self.stockFlag = false
        // console.log("STOCK FLAG FALSE:---", this.stockFlag)
      }
      // console.log("STOCK REJECTED FOR STORE:---", item)
    }
    console.log("FINAL FLAG AFTER STOCK CHECK:---", self.stockFlag)
    console.log("FINAL PRODUCT AFETR ALL CHANGES:--", values);
    console.log("MULTI SELECT STORE VALUES:--", self.toppings.value);

    if (!self.stockFlag){
      console.log("----:STOCK VALUES ENTERED:---")
      self._productService.addProduct(values).then(function(result){
        console.log("PRODUCT ADDED:===", result);
          self.addProductForm.reset();
          // self.addProductForm.controls['type'].patchValue('Single Unit', {onlySelf: true});
          self.toppings.reset();
          self.getAllProducts()
          self.selectedStores = []
          // self._buildAddProductForm()
      }).catch(function(err){
        console.log(err);
      })
    }else{
      console.log("----:MUST ENETER STOCK VALUES:---")
    }

  }

  selectSubCategoty($event){
    var self = this;
    console.log("Category selected", $event);
    self.selectedCategory = $event.value
    self._productService.getSingleCategoryItems(self.selectedCategory).then(function(result){
      self.allSubCategories = [];
      for (let row of result.docs){
        self.allSubCategories.push(row)
      }
    }).catch(function(err){
      console.log(err)
    })
  }  

  deleteStores(i, selectedStores){
    console.log("DELETE CART ITEM CALLED:-", i, selectedStores)
    selectedStores.splice(i, 1)
    this.selectedStores = selectedStores
  }

  getAllCategories(){
    var self = this;
    self._productService.allProductCategories().then(function(result){
      self.allCategories = []
      result.rows.map(function(row){
        self.allCategories.push(row.doc)
      })
      console.log("Result from all product Categories:===", self.allCategories)
    }).catch(function(err){
      console.log(err)
    })
  }


  logout(){
    this._loginService.logout()
  }  

}
