import { Component, ElementRef, ViewChild } from '@angular/core';
import { LoginService } from '../../../services/login.service'
import { ProductService } from '../../../services/product.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';


@Component({
    selector: 'product-categories',
    providers: [LoginService, ProductService],
    templateUrl: 'categories.html',
    styleUrls: ['categories.css']
})

export class productcategories {
  
  public productCategoriesDB = new PouchDB('productcategories');
  addCategoryForm: FormGroup;
  allCategories: any = []

  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    this._buildAddCategoryForm();
  }


  private _buildAddCategoryForm(){
    this.addCategoryForm = this._formBuilder.group({
      name: ['', Validators.required]
    })
  }

  ngOnInit(){
    this.getAllCategories()
  }
  
  getAllCategories(){
    var self = this;
    self.allCategories = [];
    self._productService.allProductCategories().then(function(result){
      result.rows.map(function (row) { 
        self.allCategories.push(row.doc); 
      });
      console.log("ALL PRODUCT CATEGORIES:=====", self.allCategories);
    }).catch(function(err){
      console.log(err);
    })
  }
  
  addCategory(values){
    var self = this;
    self._productService.addProductCategory(values.name).then(function(result){
      console.log("CATEGORY ADDED:---------", result)
      self.addCategoryForm.reset()
      // self.addCategoryForm.controls['name'].untouched
      // self.addCategoryForm.controls['name']
      self.addCategoryForm.controls['name'].markAsPristine();
      self.addCategoryForm.controls['name'].markAsUntouched();
      self.addCategoryForm.controls['name'].updateValueAndValidity();
      self.getAllCategories()
    }).catch(function(err) {
      console.log(err)
    })
  }

  showSubCategory(item){
    console.log("Selected Category:===", item)
    this._router.navigate(['/subcategories', item._id])
  }

  goBack(){
    this._router.navigate(['adminsettings'])
  }

  logout(){
    this._loginService.logout()
  }  
}
