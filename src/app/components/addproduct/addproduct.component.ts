import { Component, ElementRef, ViewChild } from '@angular/core';
import { LoginService } from '../../services/login.service'
import { ProductService } from '../../services/product.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import {DataSource} from '@angular/cdk/collections';
import {MatPaginator} from '@angular/material';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';

@Component({
    selector: 'add-product',
    providers: [LoginService, 
                ProductService],
    templateUrl: 'addproduct.html',
    styleUrls: ['addproduct.css']
})

export class addproduct {
  
  public productsDB = new PouchDB('products');

  addProductForm: FormGroup;
  allProducts: any = [];

  allCategories: any = [];
  allSubCategories: any = [];
  selectedCategory: any;

  seasons = [
    'Weighted',
    'Single Unit'
  ];

  constructor(private _loginService: LoginService,
              private _productService: ProductService,
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    this._buildAddProductForm();
  }

  private _buildAddProductForm(){
    this.addProductForm = this._formBuilder.group({
      name: ['', Validators.required],
      batchnumber: ['', Validators.required],
      price: ['', Validators.required],
      weight: [''],
      category: ['', Validators.required],
      stockvalue: ['', Validators.required],
      subcategory: [''],
      type: ['Single Unit', Validators.required]

    })
  }

  ngOnInit(){
    this.getAllCategories()
  }

  getAllCategories(){
    var self = this;
    self._productService.allProductCategories().then(function(result){
      self.allCategories = []
      result.rows.map(function(row){
        self.allCategories.push(row.doc)
      })
      console.log("Result from all product Categories:===", self.allCategories)
    }).catch(function(err){
      console.log(err)
    })
  }

  addProduct(values){
    var self = this;
    console.log("ADD PRODUCT CALLED:--", values);
    self._productService.addProduct(values).then(function(result){
      console.log("PRODUCT ADDED:===", result);
        self._buildAddProductForm();
        window.location.reload
    }).catch(function(err){
      console.log(err);
    })
  }

  selectSubCategoty($event){
    var self = this;
    console.log("Category selected", $event);
    self.selectedCategory = $event.value
    self._productService.getSingleCategoryItems(self.selectedCategory).then(function(result){
      self.allSubCategories = [];
      for (let row of result.docs){
        self.allSubCategories.push(row)
      }
    }).catch(function(err){
      console.log(err)
    })
  }

  goBack(){
    this._router.navigate(['products'])
  }

  logout(){
    this._loginService.logout()
  }  
}