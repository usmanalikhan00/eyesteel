import { Component, ElementRef } from '@angular/core';
import { LoginService } from '../../services/login.service'
import { AccountTypesService } from '../../services/accounttypes.service'
// import { User } from '../../models/model-index'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';

@Component({
    selector: 'cash-settings',
    providers: [LoginService, AccountTypesService],
    templateUrl: 'cashsettings.html',
    styleUrls: ['cashsettings.css']
})

export class cashsettings {
  

  addAccountTypeForm: FormGroup;
  allAccountTypes: any = []
  // companyInfoFlag: boolean = false;
  // companyInfo: any = null
  // companyId: any = null
  // companyRev: any = null
  // companyName: any = null
  // companyAddress: any = null
  // companyIntro: any = null
  // companyPhone: any = null
  // companyWebsite: any = null
  // companyDealsIn: any = null
  // companyNtn: any = null
  allTypes = [
    {
      'name':'Bank',
      'type':'bank'
    },
    {
      'name':'Cash',
      'type':'cash'
    },
    {
      'name':'Post Dated Cheques',
      'type':'post_dated_cheque'
    },
    {
      'name':'Written Document',
      'type':'written_doc'
    },
    {
      'name':'Card',
      'type':'card'
    },
    {
      'name':'Cheques',
      'type':'cheques'
    }
  ]

  constructor(private _loginService: LoginService,
              private _accountTypesService: AccountTypesService, 
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    this._buildAddAccountTypeForm();
  }

  private _buildAddAccountTypeForm(){
    this.addAccountTypeForm = this._formBuilder.group({
      name: ['', Validators.required],
      type: ['', Validators.required],
      accountnumber: ['', Validators.required],
      openingbalance: ['', Validators.required]
    })
  }

  ngOnInit(){
    this.getAccountTypes()
      console.log("Reuslt after fetching all the company info:---", this.allTypes)
  }

  getAccountTypes(){
    var self = this;
    self._accountTypesService.getAccountTypes().then(function(result){
      console.log("Reuslt after fetching all the company info:---", result)
      self.allAccountTypes = []
      result.rows.map(function (row) { 
        self.allAccountTypes.push(row.doc); 
      });
      console.log("ALL ACCOUNT TYPES:(((((((((((", self.allAccountTypes)
    }).catch(function(err){
      console.log(err)
    })    
  }

  addAccountType(values, $event){
    var self = this;
    console.log("VALUES FROM ADD ACCOUNT TYPE FORM:--", values)
    self._accountTypesService.addAccountType(values).then(function(result){
      console.log("result after adding company info:---", result)
      self.getAccountTypes()
      self._buildAddAccountTypeForm()
    }).catch(function(err){
      console.log(err)
    })
  }

  updateCompanyInfo(values){
    var self = this;
  }

  showSubAccounts(type){
    console.log("Selected Accoun Type:===", type)
    this._router.navigate(['/subaccounts', type._id])
  }

  logout(){
    this._loginService.logout()
  }  

}