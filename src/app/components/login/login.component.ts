import {Component, ElementRef} from '@angular/core';
import {LoginService} from '../../services/login.service'
// import { User } from '../../models/model-index'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';

@Component({
    selector: 'login-form',
    providers: [LoginService],
    templateUrl: 'login.html',
    styleUrls: ['login.css']
})

export class Login {
  
  public errorMsg = '';
  public userDB = new PouchDB('users');

  users: any = [];
  loginForm: FormGroup;
  authUser: any = null
  email: string = null;
  password: string = null;

  constructor(private _loginService: LoginService,
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    this._buildLoginForm();

  }

  private _buildLoginForm(){
    this.loginForm = this._formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit(){
    var self = this;
    self._loginService.addUsers().then(function (result) {
      console.log("RESULT AFTER ADDING:=======", result);
    }).catch(function (err) {
      console.log(err);
    });
  }

  getCredentials(values){
    var self = this;
    self.authUser = []
    console.log("GET CREDENTIALS:------", values);
    self._loginService.checkCredentials(values).then(function (doc) {
      console.log("RESULT AFTER CHECK CREDENTIALS:+++", doc)
      // if (doc.docs[0]){
      //   self.authUser = doc.docs[0]
      //   self.navigateUser(doc.docs[0]._id, self.authUser)
      // }
      for (let row of doc.docs){
        self.authUser.push(row)
      }
      // console.log("AUTH USER:--------", self.authUser)
      // console.log("AUTH USER:--------", self.authUser[0]._id)
      // console.log("AUTH USER:--------", (self.authUser[0]._id === "admin"))
      localStorage.setItem('user', JSON.stringify(self.authUser[0]))
      var user = JSON.parse(localStorage.getItem('user'))
      console.log("LOCAL STORAGE ID:--------", user)

      // localStorage.setItem('userRole', self.authUser[0]._id)
      // var userRole = localStorage.getItem('userRole')
      // console.log("LOCAL STORAGE USER ROLE:--------", userRole, "TYPE OF:--", typeof(userRole))

      if (user._id === "admin"){
        self._router.navigate(['admindashboard'])
      }
      if (user._id === "user"){
        self._router.navigate(['userdashboard'])
      }
      if (user._id === "deo"){
        self._router.navigate(['deodashboard'])
      }

    }).catch(function (err) {
      console.log(err);
    });
  }

  navigateUser(userId, user){
    console.log("USER ID TO NAVIGATE", userId, user);
      this._router.navigate(['admindashboard'])
    // if (userId === "admin"){
    //   console.log("ADMIN LOGGED INL-----", typeof(userId), userId);
    //   console.log("TRUE ADMIN")
    //   // this._router.navigate(['admindashboard'])
    //   // location.href = '#/admindashboard'
    // }
    // if (userId === "user"){
    //   console.log("ADMIN LOGGED INL-----", typeof(userId), userId);
    //   // this._router.navigate(['admindashboard'])
    //   console.log("TRUE USER")
    // }
    // if (userId === "deo"){
    //   console.log("ADMIN LOGGED INL-----", typeof(userId), userId);
    //   // this._router.navigate(['admindashboard'])
    //   console.log("TRUE DEO")
    // }

  }
}