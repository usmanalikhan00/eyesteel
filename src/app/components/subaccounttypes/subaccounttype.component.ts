import { Component, ElementRef, ViewChild } from '@angular/core';
import { LoginService } from '../../services/login.service'
import { AccountTypesService } from '../../services/accounttypes.service'
import { Router, ActivatedRoute, ParamMap } from  '@angular/router';
import * as PouchDB  from 'pouchdb';
import * as PouchFind from 'pouchdb-find';
PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
// import * as moment from "moment";
import 'rxjs/add/operator/switchMap';
import { Subscription } from 'rxjs';

@Component({
    selector: 'sub-account-type',
    providers: [LoginService, AccountTypesService],
    templateUrl: 'subaccounttype.html',
    styleUrls: ['subaccounttype.css']
})

export class subAccountType {
  
  public productSubCategoriesDB = new PouchDB('productsubcategories');

  addBankAccountForm: FormGroup;
  addWrittenDocAccountForm: FormGroup;
  addPostDatedAccountForm: FormGroup;
  addChequeAccountForm: FormGroup;
  addCardAccountForm: FormGroup;
  addCashAccountForm: FormGroup;
  
  allSubAccounts: any = []
  sub: any;
  accountId: any;
  selectedAccount: any;
  accountType: any = null

  constructor(private _loginService: LoginService,
              private _accountTypesService: AccountTypesService, 
              private _formBuilder: FormBuilder,
              private _activatedRouter: ActivatedRoute, 
              private _router: Router) {
    this._buildAddBankAccountForm();
    this._buildAddWrittenDocAccountForm()
    this._buildAddPostDatedAccountForm()
    this._buildAddChequeAccountForm()
    this._buildAddCardAccountForm()
    this._buildAddCashAccountForm()

  }


  _buildAddBankAccountForm(){
    this.addBankAccountForm = this._formBuilder.group({
      name: ['', Validators.required],
      accountnumber: ['', Validators.required],
      openingbalance: ['', Validators.required]
    })
  }
  _buildAddWrittenDocAccountForm(){
    this.addWrittenDocAccountForm = this._formBuilder.group({
      name: ['', Validators.required],
      openingbalance: ['', Validators.required]
    })
  }
  _buildAddPostDatedAccountForm(){
    this.addPostDatedAccountForm = this._formBuilder.group({
      name: ['', Validators.required],
      openingbalance: ['', Validators.required]
    })
  }
  _buildAddChequeAccountForm(){
    this.addChequeAccountForm = this._formBuilder.group({
      name: ['', Validators.required],
      openingbalance: ['', Validators.required]
    })
  }
  _buildAddCardAccountForm(){
    this.addCardAccountForm = this._formBuilder.group({
      name: ['', Validators.required],
      openingbalance: ['', Validators.required]
    })
  }
  _buildAddCashAccountForm(){
    this.addCashAccountForm = this._formBuilder.group({
      name: ['', Validators.required],
      openingbalance: ['', Validators.required]
    })
  }





  ngOnInit(){
    var self = this;
    self.sub = self._activatedRouter.params.subscribe(params => {
      self.accountId = params['accountId']; // (+) converts string 'id' to a number
      console.log("ACCount ID TO ADD SUB ACCOUNT:-----", self.accountId)
      self.getAllSubAccounts()

    });
  }
  
  getAllSubAccounts(){
    var self = this;
    self._accountTypesService.getSingleAccount(self.accountId).then(function(result){
      // console.log("RESULT FROM SINLE CATEGORY", result)
      self.selectedAccount = result.docs[0]
      console.log("RESULT FROM SINLE ACCOUNT", self.selectedAccount)
      self.accountType = self.selectedAccount.type
      // if (self.accountType === 'bank'){
      //   self._buildAddBankAccountForm()
      // }
      self._accountTypesService.getSingleAccountItems(self.accountId).then(function(doc){
        console.log(doc)
        self.allSubAccounts = [];
        for (let row of doc.docs){
          self.allSubAccounts.push(row)
        }
        console.log("ALL SUB ACCOUNT OF THIS TYPE:----", self.allSubAccounts)
      }).catch(function(err){
        console.log(err)
      })
    }).catch(function(err){
        console.log(err)
    })

  }

  addSubAccount(values){
    var self = this;
    console.log("PRODUCT SUB CATEGORY to add:=====", values);
    self._accountTypesService.addSubAccount(values.name, self.accountId, self.selectedAccount.type).then(function(result){
      console.log("CATEGORY ADDED:---------", result)
      self._buildAddBankAccountForm()
      self.getAllSubAccounts()
    }).catch(function(err) {
      console.log(err)
    })
  }

  goBack(){
    this._router.navigate(['cashsettings'])
  }

  logout(){
    this._loginService.logout()
  }  

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
