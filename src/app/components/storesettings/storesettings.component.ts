import { Component, ElementRef } from '@angular/core';
import { LoginService } from '../../services/login.service'
import { StoreSettingsService } from '../../services/storesettings.service'
// import { User } from '../../models/model-index'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';

@Component({
    selector: 'store-settings',
    providers: [LoginService, StoreSettingsService],
    templateUrl: 'storesettings.html',
    styleUrls: ['storesettings.css']
})

export class storesettings {
  

  addNewStoreForm: FormGroup;
  allStores: any = []


  constructor(private _loginService: LoginService,
              private _storeSettingsService: StoreSettingsService, 
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    this._buildAddStoreForm();
  }

  private _buildAddStoreForm(){
    this.addNewStoreForm = this._formBuilder.group({
      name: ['', Validators.required]
    })
  }

  ngOnInit(){
    this.getAllStores()
  }

  getAllStores(){
    var self = this;
    self._storeSettingsService.allStores().then(function(result){
      console.log("Reuslt after fetching all the company info:---", result)
      self.allStores = []
      result.rows.map(function (row) { 
        self.allStores.push(row.doc); 
      });
      console.log("ALL STORES:(((((((((((", self.allStores)
    }).catch(function(err){
      console.log(err)
    })    
  }

  addStore(values, $event){
    var self = this;
    console.log("VALUES FROM ADD Store:--", values)
    self._storeSettingsService.addStore(values).then(function(result){
      console.log("result after adding Store:---", result)
      self.getAllStores()
      self._buildAddStoreForm()
    }).catch(function(err){
      console.log(err)
    })
  }


  logout(){
    this._loginService.logout()
  }  

}