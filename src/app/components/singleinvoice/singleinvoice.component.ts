import { Component, ElementRef, ViewChild } from '@angular/core';
import { LoginService } from '../../services/login.service'
import { ProductService } from '../../services/product.service'
import { InvoiceService } from '../../services/invoice.service'
import { Router, ActivatedRoute, ParamMap } from  '@angular/router';
import * as PouchDB  from 'pouchdb';
import * as PouchFind from 'pouchdb-find';
PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
// import * as moment from "moment";
import 'rxjs/add/operator/switchMap';
import { Subscription } from 'rxjs';

@Component({
    selector: 'single-invoice',
    providers: [LoginService, ProductService, InvoiceService],
    templateUrl: 'singleinvoice.html',
    styleUrls: ['singleinvoice.css']
})

export class singleinvoice {
  
  // public productSubCategoriesDB = new PouchDB('productsubcategories');

  // addSubCategoryForm: FormGroup;
  // allSubCategories: any = []
  sub: any;
  invoiceId: any;
  selectedInvoice: any;
  invoiceNotes: any;

  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _invoiceService: InvoiceService, 
              private _formBuilder: FormBuilder,
              private _activatedRouter: ActivatedRoute, 
              private _router: Router) {
    this._buildAddSubCategoryForm();
  }


  private _buildAddSubCategoryForm(){
    // this.addSubCategoryForm = this._formBuilder.group({
    //   name: ['', Validators.required]
    // })
  }
  // ngOnInit() {
  //   this.sub = this._activatedRouter.params.subscribe(params => {
  //      this.categoryId = params['categoryId']; // (+) converts string 'id' to a number
  //   });
  // }
      // this._route.paramMap
      // .switchMap((params: ParamMap) =>
      //   this._demandService.updateDemandStatus(this.demands[i]._id,demState))
      // .subscribe(demands =>{
      //   console.log("Dem Status Resp",demands)
      // });

  ngOnInit(){
    var self = this;
    // this._activatedRouter.paramMap
    //   .switchMap((params: ParamMap) => )
    self.sub = self._activatedRouter.params.subscribe(params => {
      self.invoiceId = params['invoiceId']; // (+) converts string 'id' to a number
      console.log("CATEGORY ID TO ADD SUB CATEGORY:-----", self.invoiceId)
      // self._productService.getSingleCategory(this.categoryId).then(function (doc) {
      //   // handle doc
      // }).catch(function (err) {
      //   console.log(err);
      // })
      self.getSingleInvoice()

    });
  }
  
  getSingleInvoice(){
    var self = this;
    self._invoiceService.getSingleInvoice(self.invoiceId).then(function(result){
      // console.log("RESULT FROM SINLE CATEGORY", result)
      self.selectedInvoice = result.docs[0]
      console.log("RESULT FROM SINLE CATEGORY", self.selectedInvoice)
      // self.invoiceNotes = self.selectedInvoice.invoicenotes
      // self._productService.getSingleCategoryItems(self.invoiceId).then(function(doc){
      //   console.log(doc)
      //   self.allSubCategories = [];
      //   // self.selectedCategory = doc;
      //   for (let row of doc.docs){
      //     self.allSubCategories.push(row)
      //   }
      //   console.log("ALL SUB CATEGORIES OF THIS CATEGORY:----", self.allSubCategories)
      // }).catch(function(err){
      //   console.log(err)
      // })
    }).catch(function(err){
        console.log(err)
    })
    // self._productService.allProductSubCategories().then(function(result){
    //   result.rows.map(function (row) { 
    //     self.allSubCategories.push(row.doc); 
    //   });
    //   console.log("ALL PRODUCT SUB CATEGORIES:=====", self.allSubCategories);
    // }).catch(function(err){
    //   console.log(err);
    // })
  }

  // addSubCategory(values){
  //   var self = this;
  //   console.log("PRODUCT SUB CATEGORY to add:=====", values);
  //   self._productService.addProductSubCategory(values.name, self.categoryId).then(function(result){
  //     console.log("CATEGORY ADDED:---------", result)
  //     self._buildAddSubCategoryForm()
  //     self.getAllSubCategories()
  //   }).catch(function(err) {
  //     console.log(err)
  //   })
  // }

  goBack(){
    this._router.navigate(['invoice'])
  }

  logout(){
    this._loginService.logout()
  }  

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
