import { Component, ElementRef, ViewChild } from '@angular/core';
import { LoginService } from '../../../services/login.service'
import { ProductService } from '../../../services/product.service'
import { StoreSettingsService } from '../../../services/storesettings.service'
import { CustomersService } from '../../../services/customers.service'
import { InvoiceService } from '../../../services/invoice.service'
import { PurchaseService } from '../../../services/purchase.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
import * as moment from "moment";
import * as _ from "lodash";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';

@Component({
    selector: 'add-purchase',
    providers: [LoginService, 
                ProductService, 
                StoreSettingsService, 
                InvoiceService, 
                PurchaseService, 
                CustomersService],
    templateUrl: 'addpurchase.html',
    styleUrls: ['addpurchase.css']
})

export class addpurchase {
  
  // public productsDB = new PouchDB('steelinvoice');
  public purchaseDB = new PouchDB('steelcustomerpurchase');
  public productsDB = new PouchDB('steelproducts');

  users: any = [];
  invoiceProducts: any = [];
  allInvoices: any= [];
  
  addInvoiceForm: FormGroup;
  addCustomerForm: FormGroup;
  addStoreForm: FormGroup;
  addProductForm: FormGroup;
  
  storeStates: Observable<any[]>;
  allStores: any = []
  selectedStore: any = null
  stores: any = []
  
  customerStates: Observable<any[]>;
  customers: any[] = []
  allCustomers: any[] = []
  selectedCustomer: any = null
  
  filteredStates: Observable<any[]>;
  states: any[] = []
  
  cartProducts: any[] = []
  cartTotal: any = 0
  
  cartStates: Observable<any[]>;
  purchaseNotes: any;

  newProductFlag: boolean = false
  newProductStores: any = []

  productTypes = [
    'Weighted',
    'Single Unit'
  ];
  // formstores =  new FormControl()

  allCategories: any = [];
  allSubCategories: any = [];
  selectedCategory: any = null;
  selectedStores: any = [];
  stockFlag: boolean = false;

  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _storeSettingsService: StoreSettingsService, 
              private _customersService: CustomersService, 
              private _invoiceService: InvoiceService, 
              private _purchaseService: PurchaseService, 
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    this._buildAddInvoiceForm();
    this._buildAddCustomerForm();
    this._buildAddStoreForm();
    this._buildAddProductForm();
  }


  private _buildAddInvoiceForm(){
    this.addInvoiceForm = this._formBuilder.group({
      stateCtrl: ['']
    })
  }
  private _buildAddCustomerForm(){
    this.addCustomerForm = this._formBuilder.group({
      customerCtrl: ['']
    })
  }
  private _buildAddStoreForm(){
    this.addStoreForm = this._formBuilder.group({
      storeCtrl: ['']
    })
  }

  private _buildAddProductForm(){
    this.addProductForm = this._formBuilder.group({
      name: ['', Validators.required],
      type: ['', Validators.required],
      category: ['', Validators.required],
      subcategory: [''],
      price: ['', Validators.required],
      weight: ['', Validators.required],
      formstores:['', Validators.required]
    })
  }



  ngOnInit(){
    this.getCartProducts()
    this.getAllStores()
    this.getAllCustomers()
    this.getAllCategories()
    // this.allSubCategories()
  }
  
  getAllCategories(){
    var self = this;
    self._productService.allProductCategories().then(function(result){
      self.allCategories = []
      result.rows.map(function(row){
        self.allCategories.push(row.doc)
      })
      console.log("Result from all product Categories:===", self.allCategories)
    }).catch(function(err){
      console.log(err)
    })
  }


  selectSubCategory($event){
    var self = this;
    console.log("Category selected", $event);
    self.selectedCategory = $event.value
    self._productService.getSingleCategoryItems(self.selectedCategory).then(function(result){
      self.allSubCategories = [];
      for (let row of result.docs){
        self.allSubCategories.push(row)
      }
    }).catch(function(err){
      console.log(err)
    })
  }


  getAllCustomers(){
    var self = this;
    self.allCustomers = [];
    self._customersService.allCustomers().then(function(result){
      result.rows.map(function (row) { 
        self.allCustomers.push(row.doc); 
      });

      self.customers = []
      for (let item of self.allCustomers){
        
          var object = {
            "name":item.name,
            "_id":item._id,
            "_rev":item._rev,
            "address":"",
            "phone":"",
            "email":""
          }          

        self.customers.push(object)
      }
      self.customerStates = self.addCustomerForm.controls['customerCtrl'].valueChanges
          .startWith(null)
          .map(state => state ? self.filterCustomers(state) : self.customers.slice());
      console.log("ALL CUSTOMERS:=====", self.allCustomers);
    }).catch(function(err){
      console.log(err);
    })
  }

  getPurchaseNotes($event){
    this.purchaseNotes = $event
    console.log("INVOICE NOTES ARE:-----", $event ,this.purchaseNotes)
  }

  getAllStores(){
    var self = this
    self.allStores = []
    self.newProductStores = []
    self._storeSettingsService.allStores().then(function(result){
      result.rows.map(function (row) { 
        self.allStores.push(row.doc); 
        self.newProductStores.push(row.doc)
      });
      self.storeStates = self.addStoreForm.controls['storeCtrl'].valueChanges
          .startWith(null)
          .map(state => state ? self.filterStores(state) : self.allStores.slice());
      console.log("ALL Stores:=====", self.allStores);
      console.log("ALL NEW PRODUCT Stores:=====", self.newProductStores);
    }).catch(function(err){

    })
  }
  
  getCartProducts(){
    var self = this;
    // self.stateCtrl = new FormControl();
    self.invoiceProducts = [];
    self._productService.allProducts().then(function(result){
      result.rows.map(function (row) { 
        self.invoiceProducts.push(row.doc); 
      });
      // console.log("ALL PRODUCTS:=====", self.invoiceProducts);
      self.states = [];
      for (let item of self.invoiceProducts){
        
          var object = {
            "name":item.name,
            "weight":item.weight,
            "batchnumber":item.batchnumber,
            "stockvalue":item.stockvalue,
            "_id":item._id,
            "_rev":item._rev,
            "category":item.category,
            "subcategory":item.subcategory,
            "price":item.price,
            "cartweight":item.weight,
            "calcweight":item.weight,
            "type":item.type,
            "stores":item.stores,
            "cartstores":"",
            "cartquantity":0,
            "cartprice":0
          }          

        self.states.push(object)
      }
      var newProduct = {
        "type":"new_product",
        "name":"New Product",
        "price":"",
        "stockvalue":""

      }
      self.states.push(newProduct)
      self.filteredStates = self.addInvoiceForm.controls['stateCtrl'].valueChanges
          .startWith(null)
          .map(state => state ? self.filterStates(state) : self.states.slice());
    }).catch(function(err){
      console.log(err);
    })
  }

  addPurchase(){
    var self = this;
    var user = JSON.parse(localStorage.getItem('user'))
    var ID = Math.floor(Math.random()*1000) + 100000
    self.selectedCustomer.balance = self.cartTotal
    var purchaseDoc = {
      "purchasenumber": ID, 
      "purchasetotal": self.cartTotal, 
      "purchaseproducts": self.cartProducts, 
      "purchasecustomer": self.selectedCustomer, 
      "status": null, 
      "createdby": user, 
      "createdat": moment().format(),
      "purchasenotes":self.purchaseNotes 
    }
    console.log("PURCHASE DOC TO PUSH:=====", purchaseDoc);
    self._purchaseService.addPurchase(purchaseDoc).then(function(result){
      console.log("RESULT AFTER ADDING PURCHASE=======", result)
      var productDocs: any = []
      var purchaseDocs: any = []
      for (let product of self.invoiceProducts){
        for (let doc of self.cartProducts){
          if (product._id === doc._id){
            var sum = 0
            for (let obj of doc.stores){
              if (obj._id === doc.cartstores._id){
                obj.stock = obj.stock + doc.cartquantity
              }  
            // product.stores = doc.stores
              sum = sum + obj.stock
            }
            // console.log("NEW PRODUCT:=============", product)
            // product.stores = doc.stores
            doc.totatcartweight = doc.cartquantity * doc.weight
            // product.stores = doc.stores
            // doc.stockvalue = product.stockvalue - doc.cartquantity
            doc.stockvalue = sum
            if (product.type === "Weighted"){
              doc.netweight = doc.stockvalue * doc.weight
              doc.networth = doc.stockvalue * doc.weight * doc.price
            }
            if (product.type === "Single Unit"){
              doc.networth = doc.stockvalue * product.price
            }
            purchaseDocs.push(doc)
            productDocs.push(product)
          }
        }
      }
      console.log("PRODUCT DOCS TO UPDATE:=====", productDocs);
      console.log("PRODUCT DOCS IN PURCHASE:=====", purchaseDocs);
      // PouchDB.replicate(self.invoicesDB, 'http://localhost:5984/steelinvoices', {live: true});
      self._productService.updateInvoiceProducts(purchaseDocs).then(function(result){
        console.log("RESULT AFTER UPDATEING PURCHASE PRODUCTS:--------", result)
        // PouchDB.replicate(self.productsDB, 'http://localhost:5984/steelproducts', {live: true});
      }).catch(function(err){
        console.log(err)
      })
    }).catch(function(err){
      console.log(err)
    })

  }




  // selectProductStore(product, state){
  //   for (let store of product.stores){
  //     if (store._id === state._id){
  //       product.cartstores = state
  //     }
  //   }
  //   console.log("SELCTED STORE ON SELCTED PRODUCT", product)
  //   var sum = 0
  //   for (let store of product.stores){
  //     if (store._id === state._id){
  //       console.log("STORE MATCHED:---", store)
  //       store.stock = store.stock - product.cartquantity
  //       product.stockvalue = product.stockvalue - product.cartquantity
  //     }
  //   }
  // }

  setProductStore($event, product){
    // this.selectedStore = $event.value
    // console.log("Selected Store:----", $event.value)
    for (let store of product.stores){
      if (store._id === $event.value){
        product.cartstores = store
        // this.selectedStore = store
      }
    }
    // product.cartstores = this.selectedStore
  }

  selectCustomer(state){

    this.selectedCustomer = state
    // console.log("SELECTED CUSTOMER:=====", this.selectedCustomer)
    this._buildAddCustomerForm()
    this.getAllCustomers()
  }




  changeCustomer($event, selectedCustomer, query){

    selectedCustomer = $event
    console.log("SELECTED CUSTOMER:=====", this.selectedCustomer)
  }


  addNewProduct(values){

    var self = this;
    console.log("VALUES FROM ADD PRODUCT FORM:", values)
    
    self.selectedStores = []
    for (let store of values.formstores){
      store.stock = 0
    }
    self.selectedStores = values.formstores

    var ID = Math.floor(Math.random()*100000000) + 800000000
    console.log("THE GENERATE NUMBER 3:-----", ID)
    values.batchnumber = ID
    console.log("ADD PRODUCT CALLED:--", values, self.selectedStores);
    values.stores = self.selectedStores
    var stockCount = 0
    for (let store of self.selectedStores){
      stockCount = stockCount + store.stock
    }
    values.stockvalue = stockCount
    values.storeStocks = stockCount

    if (values.type === 'Weighted'){
      values.netweight = values.weight * values.stockvalue
      values.networth = values.netweight * values.price
    }else if (values.type === 'Single Unit'){
      values.netweight = ""
      values.networth = values.stockvalue * values.price

    }

    for (let item of self.selectedStores){
      if (item.stock === null){
        self.stockFlag = true
        // console.log("STOCK FLAG TRUE:---", this.stockFlag)
        break
      }else{
        self.stockFlag = false
        // console.log("STOCK FLAG FALSE:---", this.stockFlag)
      }
      // console.log("STOCK REJECTED FOR STORE:---", item)
    }
    console.log("FINAL FLAG AFTER STOCK CHECK:---", self.stockFlag)
    console.log("FINAL PRODUCT AFETR ALL CHANGES:--", values);
    // console.log("MULTI SELECT STORE VALUES:--", this.toppings.value);

    self._productService.addProduct(values).then(function(result){
      console.log("RESULT AFTER ADDING THE PRODUCT:-------", result)
      self.addProductForm.reset()
      self.getCartProducts()
      self.newProductFlag = false
    }).catch(function(err){
      console.log(err)
    })
  }


  selectCartProduct(item){
    if (item.type === 'new_product'){
      this.newProductFlag = true
      this._buildAddInvoiceForm()
      this.getCartProducts()
    }else{
      // console.log("AUTO SELCT CHANGE FUNCTION", item)
      this.selecteProduct(item)
      // this._buildAddInvoiceForm()
    }

  }


  selecteProduct(product){
    console.log("CHANGE INPUT VALUE SELECED:_+_", product)
    var flag = false;
    if (product){
      if (this.cartProducts.length == 0){
        product.cartquantity = product.cartquantity + 1
        if (product.type == 'Weighted'){
          product.calcweight = product.cartquantity * product.cartweight
          product.cartworth = product.cartquantity * product.cartweight * product.cartprice
        }else{
          product.calcweight = null
          product.cartworth = product.cartquantity * product.cartprice
        }
        this.cartProducts.push(product)
        this.calculateTotal(this.cartProducts)
      }else{
        for (let i=0; i<this.cartProducts.length; i++){
          if (product._id === this.cartProducts[i]._id){
            this.cartProducts[i].cartquantity = this.cartProducts[i].cartquantity + 1
            if (this.cartProducts[i].type == 'Weighted'){
              this.cartProducts[i].calcweight = this.cartProducts[i].cartquantity * this.cartProducts[i].cartweight
              this.cartProducts[i].cartworth = this.cartProducts[i].cartquantity * this.cartProducts[i].cartweight * this.cartProducts[i].cartprice
            }else{
              this.cartProducts[i].calcweight = null
              this.cartProducts[i].cartworth = this.cartProducts[i].cartquantity * this.cartProducts[i].cartprice
            }

            this.calculateTotal(this.cartProducts)
            flag = true
            console.log("MATCHED VALUE_+++", flag)
            break
          }
        }
        if (!flag){
          product.cartquantity = product.cartquantity + 1
          if (product.type == 'Weighted'){
            product.calcweight = product.cartquantity * product.cartweight
            product.cartworth = product.cartquantity * product.cartweight * product.cartprice
          }else{
            product.calcweight = null
            product.cartworth = product.cartquantity * product.cartprice
          }
          this.cartProducts.push(product)
          this.calculateTotal(this.cartProducts)
        }
      }

      this._buildAddInvoiceForm()
      this.getCartProducts()
    }
    
  }

  changeWeight(weight, product){
    product.cartweight = weight
    product.calcweight = product.cartquantity * product.cartweight
    product.cartworth = product.calcweight * product.cartprice
    // console.log("PRODUCT WHICH WEIGHT CHANGED:-", product)
    // console.log("********PRODUCT CALCULATED WEIGHT IS************:-", product.calcweight)
    // console.log("ALL PRODUCTS FROM CART WEIGHT CHANGE  :-", this.cartProducts)
    if (weight <= 0){
      product.cartweight = product.weight
      product.calcweight = product.cartquantity * product.cartweight
      product.cartworth = product.calcweight * product.cartprice
    }
    this.calculateTotal(this.cartProducts)
  }

  changeQuantity(quantity, product){
    
    console.log("QUANTITY ((((((()))))))):-", quantity)
    if (quantity >= 1){
      product.cartquantity = quantity
      // product.cartprice = product.cartquantity * product.cartprice
      if (product.type == 'Weighted'){
        product.calcweight = product.cartquantity * product.cartweight
        product.cartworth = product.cartquantity * product.cartweight * product.cartprice
      }else{
        product.calcweight = null
        product.cartworth = product.cartquantity * product.cartprice
      }
      // console.log("PRODUCT WHICH QUANITY CHANGED:-", product)
      // console.log("ALL PRODUCTS FROM CART AFETR QUANITTY CHANGE:-", this.cartProducts)
      this.calculateTotal(this.cartProducts)
    }
    if (quantity <= 0){
      product.cartquantity = 1
    }
  }

  changePrice(price, product){
    product.cartprice = price
    if (product.type == 'Weighted'){
      product.calcweight = product.cartquantity * product.cartweight
      product.cartworth = product.cartquantity * product.cartweight * product.cartprice
    }else{
      product.calcweight = null
      product.cartworth = product.cartquantity * product.cartprice
    }

    // product.cartprice = product.cartquantity * product.price
    // product.cartprice = quantity
    // console.log("PRODUCT WHICH QUANITY CHANGED:-", product)
    // console.log("ALL PRODUCTS FROM CART AFETR QUANITTY CHANGE:-", this.cartProducts)
    this.calculateTotal(this.cartProducts)
  }




  calculateTotal(carItems){
    var total = 0  
    for (let item of carItems){
      total = total + item.cartworth
    }
    this.cartTotal = total
    // this.cartTotal = this.cartTotal.toLocaleString("en")
    // console.log("TOTAL VALUE OF CART:-", this.cartTotal)
  }

  deleteCartItem(i, cartProducts){
    // console.log("DELETE CART ITEM CALLED:-", i, cartProducts)
    cartProducts.splice(i, 1)
    this.cartProducts = cartProducts
    this.calculateTotal(this.cartProducts)

  }


  filterStates(name: string) {
    console.log("ALL PRODUCTS:=====", this.addInvoiceForm.controls['stateCtrl'].value);
    return this.states.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }

  filterCustomers(name: string) {
    console.log("ALL Customers:=====", this.addCustomerForm.controls['customerCtrl'].value);
    return this.customers.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }

  filterStores(name: string) {
    console.log("ALL Customers:=====", this.addStoreForm.controls['storeCtrl'].value);
    return this.stores.filter(state =>
      state.name.toLowerCase().indexOf(name.toLowerCase()) > -1);
  }


  logout(){
    this._loginService.logout()
  }

}
