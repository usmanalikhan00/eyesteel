import { Component, ElementRef, ViewChild } from '@angular/core';
import { LoginService } from '../../services/login.service'
import { ProductService } from '../../services/product.service'
import { InvoiceService } from '../../services/invoice.service'
import { PurchaseService } from '../../services/purchase.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';


@Component({
    selector: 'purchase',
    providers: [LoginService, ProductService, InvoiceService, PurchaseService],
    templateUrl: 'purchase.html',
    styleUrls: ['purchase.css']
})

export class purchase {
  
  public purchaseDB = new PouchDB('steelcustomerpurchase');

  
  allPurchases: any= [];

  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _invoiceService: InvoiceService,
              private _purchaseService: PurchaseService, 
              private _formBuilder: FormBuilder, 
              private _router: Router) {
  }

  ngOnInit(){
    var self = this;
    self.purchaseDB.replicate.from('http://localhost:5984/steelpurchases', {live: true});
    self.allPurchases = [];
    self._purchaseService.allPurchases().then(function(result){
      result.rows.map(function (row) { 
        self.allPurchases.push(row.doc); 
      });
      self.purchaseDB.replicate.to('http://localhost:5984/steelpurchases', {live: true});

      console.log("ALL PURCHASES:=====", self.allPurchases);
    }).catch(function(err){
      console.log(err);
    })
  }


  getSinglePurchase(purchase){
    console.log("Selected Purchase:===", purchase)
    this._router.navigate(['/purchase', purchase._id])
  }

  logout(){
    this._loginService.logout()
  }  
}
