import { Component, ElementRef, ViewChild } from '@angular/core';
import { LoginService } from '../../../services/login.service'
import { PurchaseService } from '../../../services/purchase.service'
import { ProductService } from '../../../services/product.service'
import { InvoiceService } from '../../../services/invoice.service'
import { Router, ActivatedRoute, ParamMap } from  '@angular/router';
import * as PouchDB  from 'pouchdb';
import * as PouchFind from 'pouchdb-find';
PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
// import * as moment from "moment";
import 'rxjs/add/operator/switchMap';
import { Subscription } from 'rxjs';

@Component({
    selector: 'single-purchase',
    providers: [LoginService, ProductService, InvoiceService, PurchaseService],
    templateUrl: 'singlepurchase.html',
    styleUrls: ['singlepurchase.css']
})

export class singlepurchase {
  
  // public productSubCategoriesDB = new PouchDB('productsubcategories');

  // addSubCategoryForm: FormGroup;
  // allSubCategories: any = []
  sub: any;
  purchaseId: any;
  selectedPurchase: any;
  purchaseNotes: any;

  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _invoiceService: InvoiceService, 
              private _purchaseService: PurchaseService, 
              private _formBuilder: FormBuilder,
              private _activatedRouter: ActivatedRoute, 
              private _router: Router) {
    this._buildAddSubCategoryForm();
  }


  private _buildAddSubCategoryForm(){
    // this.addSubCategoryForm = this._formBuilder.group({
    //   name: ['', Validators.required]
    // })
  }
  // ngOnInit() {
  //   this.sub = this._activatedRouter.params.subscribe(params => {
  //      this.categoryId = params['categoryId']; // (+) converts string 'id' to a number
  //   });
  // }
      // this._route.paramMap
      // .switchMap((params: ParamMap) =>
      //   this._demandService.updateDemandStatus(this.demands[i]._id,demState))
      // .subscribe(demands =>{
      //   console.log("Dem Status Resp",demands)
      // });

  ngOnInit(){
    var self = this;

    self.sub = self._activatedRouter.params.subscribe(params => {
      self.purchaseId = params['purchaseId']; // (+) converts string 'id' to a number
      console.log("CATEGORY ID TO ADD SUB CATEGORY:-----", self.purchaseId)
      self.getSinglePurchase()

    });
  }
  
  getSinglePurchase(){
    var self = this;
    self._purchaseService.getSinglePurchase(self.purchaseId).then(function(result){
      // console.log("RESULT FROM SINLE CATEGORY", result)
      self.selectedPurchase = result.docs[0]
      console.log("RESULT FROM SELECTED PURCHASE:---------", self.selectedPurchase)
    }).catch(function(err){
        console.log(err)
    })

  }

  goBack(){
    this._router.navigate(['purchase'])
  }

  logout(){
    this._loginService.logout()
  }  

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
