import {Component, ElementRef} from '@angular/core';
import {LoginService} from '../../services/login.service'
import {SettingsService} from '../../services/settings.service'
// import { User } from '../../models/model-index'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';

@Component({
    selector: 'company-info',
    providers: [LoginService, SettingsService],
    templateUrl: 'companyinfo.html',
    styleUrls: ['companyinfo.css']
})

export class companyinfo {
  

  addCompanyInfoForm: FormGroup;
  companyInfoFlag: boolean = false;
  companyInfo: any = null
  companyId: any = null
  companyRev: any = null
  companyName: any = null
  companyAddress: any = null
  companyIntro: any = null
  companyPhone: any = null
  companyWebsite: any = null
  companyDealsIn: any = null
  companyNtn: any = null

  constructor(private _loginService: LoginService,
              private _settingsService: SettingsService, 
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    this._buildAddCompnayInfoForm();
  }

  private _buildAddCompnayInfoForm(){
    this.addCompanyInfoForm = this._formBuilder.group({
      name: ['', Validators.required],
      intro: [''],
      website: [''],
      address: [''],
      phone: [''],
      ntn: [''],
      dealsin: ['']
    })
  }

  ngOnInit(){
    this.getCompanyInfo()
  }

  getCompanyInfo(){
    var self = this;
    self._settingsService.getCompnayInfo().then(function(result){
      console.log("Reuslt after fetching all the company info:---", result)
      result.rows.map(function (row) { 
        self.companyInfo = row.doc; 
      });
      if (result.rows.length != 0){
        self.companyId = self.companyInfo._id
        self.companyRev = self.companyInfo._rev
        self.companyName = self.companyInfo.name
        self.companyAddress = self.companyInfo.address
        self.companyIntro = self.companyInfo.intro
        self.companyPhone = self.companyInfo.phone
        self.companyWebsite = self.companyInfo.website
        self.companyDealsIn = self.companyInfo.dealsin
        self.companyNtn = self.companyInfo.ntn
      }
      console.log("COMPANY INFO TO DISPLAY:---", self.companyInfo)
    }).catch(function(err){
      console.log(err)
    })    
  }

  addCompanyInfo(values){
    var self = this;
    console.log("VALUES FROM COMPNAY INFO FORM:--", values)
    self._settingsService.addCompanyInfo(values).then(function(result){
      console.log("result after adding company info:---", result)

    }).catch(function(err){
      console.log(err)
    })
  }

  updateCompanyInfo(values){
    var self = this;
    console.log("COMPANY INFO TO EDIT:-----", values, 
      "Company INFO TOE DEDIT:---", self.companyId, self.companyRev);
    self._settingsService.updateCompanyInfo(values, self.companyId, self.companyRev).then(function(result){
      console.log("RESULT AFTER UPDATING THE COMPANY INFO:-----", result)
      self.companyInfoFlag = false;
      self.getCompanyInfo()
    }).catch(function(err){
      console.log(err)
    })
  }

  goBack(){
    this.companyInfoFlag = false;
  }

  goToEdit(){
    this.companyInfoFlag = true;
    (<FormGroup>this.addCompanyInfoForm).setValue({'name':this.companyName,
                                               'website':this.companyWebsite,
                                               'phone':this.companyPhone,
                                               'ntn':this.companyNtn,
                                               'intro':this.companyIntro,
                                               'address':this.companyAddress,
                                               'dealsin':this.companyDealsIn}, { onlySelf: true });
  }

  logout(){
    this._loginService.logout()
  }  

}