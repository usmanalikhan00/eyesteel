import { Component, ElementRef, ViewChild } from '@angular/core';
import { LoginService } from '../../services/login.service'
import { ProductService } from '../../services/product.service'
import { InvoiceService } from '../../services/invoice.service'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';


@Component({
    selector: 'invoice',
    providers: [LoginService, ProductService, InvoiceService],
    templateUrl: 'invoice.html',
    styleUrls: ['invoice.css']
})

export class invoice {
  
  public invoicesDB = new PouchDB('steelcustomerinvoice');

  
  allInvoices: any= [];

  constructor(private _loginService: LoginService,
              private _productService: ProductService, 
              private _invoiceService: InvoiceService, 
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    this._buildAddProductForm();
  }


  private _buildAddProductForm(){

  }

  ngOnInit(){
    var self = this;
    self.allInvoices = [];
    self._invoiceService.allInvocies().then(function(result){
      result.rows.map(function (row) { 
        self.allInvoices.push(row.doc); 
      });
      PouchDB.replicate(self.invoicesDB, 'http://localhost:5984/steelinvoices', {live: true});

      console.log("ALL INVOICES:=====", self.allInvoices);
    }).catch(function(err){
      console.log(err);
    })
  }


  getSingleInvoice(invoice){
    console.log("Selected Invoice:===", invoice)
    this._router.navigate(['/invoice', invoice._id])
  }

  logout(){
    this._loginService.logout()
  }  
}
