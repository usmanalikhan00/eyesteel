import {Component, ElementRef} from '@angular/core';
import {LoginService} from '../../../services/login.service'
// import { User } from '../../models/model-index'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb';
// import * as PouchFind from 'pouchdb-find';
// PouchDB.plugin(PouchFind)
// import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';

@Component({
    selector: 'admin-settings',
    providers: [LoginService],
    templateUrl: 'adminsettings.html',
    styleUrls: ['adminsettings.css']
})

export class adminsettings {
  
  constructor(private _loginService: LoginService,
              private _formBuilder: FormBuilder, 
              private _router: Router) {
    this._buildAddProductForm();

  }

  private _buildAddProductForm(){
    
  }

  goBack(){
    this._router.navigate(['admindashboard'])
  }

  logout(){
    this._loginService.logout()
  }  
}